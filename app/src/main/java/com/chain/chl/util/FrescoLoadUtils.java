package com.chain.chl.util;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

/**
 * Created by Administrator on 2016/11/14 0014.
 */

public class FrescoLoadUtils {

    private static void useOldCustomHierarchy(SimpleDraweeView simpleDraweeView) {
        GenericDraweeHierarchy genericDraweeHierarchy = simpleDraweeView.getHierarchy();
        /*设置出现时渐变时间*/
        genericDraweeHierarchy.setFadeDuration(300);
        /*设置缩放模式*//*android:scaleType 和 setScaleType() 方法，它们对 Drawees 无效。*/
//        genericDraweeHierarchy.setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
    }


    public static void setImage(SimpleDraweeView simpleDraweeView, String url) {
        setImage(simpleDraweeView, Uri.parse(url), 300, 300);
    }

    public static void setImage(SimpleDraweeView simpleDraweeView, Uri uri) {
        setImage(simpleDraweeView, uri, 300, 300);
    }


    public static void setImage(SimpleDraweeView simpleDraweeView, Uri uri, int width, int height) {
        simpleDraweeView.setController(useNewCustomController(simpleDraweeView, uri, width, height));
    }

    public static DraweeController useNewCustomController(SimpleDraweeView simpleDraweeView, Uri uri, int width, int
            height) {
        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setImageRequest(createImageRequest(uri, width, height))
                .setOldController(simpleDraweeView.getController())
                .build();
        return draweeController;
    }

    private static ImageRequest createImageRequest(Uri uri, int width, int height) {
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(width, height))
                /*JPEG渐进式显示*/
                .setAutoRotateEnabled(true)
                //.setProgressiveRenderingEnabled(true)
                /*是否根据设备来调整图片的显示角度*/
                /*设置最低请求级别*/
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        return imageRequest;
    }


    public static boolean clearImage(Uri uri) {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        boolean inMemoryCache = imagePipeline.isInBitmapMemoryCache(uri);
        if (inMemoryCache) {
            imagePipeline.evictFromCache(uri);
            return true;
        }
        return false;
    }


    public static class Builder {

        private SimpleDraweeView currentDraweeView;
        private Uri uri;
        private Uri lowResUri;
        private int animDuration;
        private Drawable placeholderDrawable;
        private int targetWidth;
        private int targetHeight;
        private ScalingUtils.ScaleType scaleType;

        private Builder() {
            animDuration = 300;
            targetWidth = 600;
            targetHeight = 600;
            scaleType = ScalingUtils.ScaleType.CENTER_CROP;
        }

        public static Builder newBuilderWithDraweeView(SimpleDraweeView simpleDraweeView) {
            return new Builder().setCurrentDraweeView(simpleDraweeView);
        }

        public Builder setCurrentDraweeView(SimpleDraweeView simpleDraweeView) {
            this.currentDraweeView = simpleDraweeView;
            return this;
        }

        public Builder setUri(Uri uri) {
            if (uri == null) {
                return this;
            }
            this.uri = uri;
            return this;
        }

        public Builder setUriStr(String uriStr) {
            if (uriStr == null) {
                return this;
            }
            this.uri = Uri.parse(uriStr);
            return this;
        }

        public Builder setLowResUri(Uri uri) {
            if (uri == null) {
                return this;
            }
            this.lowResUri = uri;
            return this;
        }

        public Builder setLowResUriStr(String uriStr) {
            if (uriStr == null) {
                return this;
            }
            this.lowResUri = Uri.parse(uriStr);
            return this;
        }

        public Builder setPlaceholderDrawable(Drawable placeholderDrawable) {
            this.placeholderDrawable = placeholderDrawable;
            return this;
        }

        public Builder setTargetHeight(int targetHeight) {
            this.targetHeight = targetHeight;
            return this;
        }

        public Builder setTargetWidth(int targetWidth) {
            this.targetWidth = targetWidth;
            return this;
        }

        public Builder setAnimDuration(int animDuration) {
            this.animDuration = animDuration;
            return this;
        }

        public Builder setScaleType(ScalingUtils.ScaleType scaleType) {
            this.scaleType = scaleType;
            return this;
        }

        public void build() {
            if (currentDraweeView != null) {
                useOldCustomHierarchy();
                currentDraweeView.setController(useNewCustomController());
            } else {
                Log.e("error", "currentDraweeView must not null");
            }
        }

        private void useOldCustomHierarchy() {
            GenericDraweeHierarchy genericDraweeHierarchy = currentDraweeView.getHierarchy();
        /*设置出现时渐变时间*/
            genericDraweeHierarchy.setFadeDuration(animDuration);
            if (placeholderDrawable != null) {
                genericDraweeHierarchy.setPlaceholderImage(placeholderDrawable);
            }
            genericDraweeHierarchy.setActualImageScaleType(scaleType);
        /*设置缩放模式*//*android:scaleType 和 setScaleType() 方法，它们对 Drawees 无效。*/
            //        genericDraweeHierarchy.setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
        }


        private DraweeController useNewCustomController() {
            DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                    .setLowResImageRequest(createImageRequest(lowResUri))
                    .setImageRequest(createImageRequest(uri))
                    .setOldController(currentDraweeView.getController())
                    .build();
            return draweeController;
        }

        /*private AbstractDraweeController useNewPipelineDraweeController() {
            PipelineDraweeControllerBuilder pipelineDraweeControllerBuilder =
                    Fresco.getDraweeControllerBuilderSupplier()
                            .get()
                            .setImageRequest(createImageRequest(uri))
                            .setLowResImageRequest(createImageRequest(lowResUri))
                            .setOldController(currentDraweeView.getController());
            return pipelineDraweeControllerBuilder.build();
        }*/


        private ImageRequest createImageRequest(Uri uri) {
            if (uri == null) {
                return null;
            }
            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .setResizeOptions(new ResizeOptions(targetWidth, targetHeight))
                /*JPEG渐进式显示*/
                    .setAutoRotateEnabled(true)
                    //.setProgressiveRenderingEnabled(true)
                /*是否根据设备来调整图片的显示角度*/
                /*设置最低请求级别*/
                    .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                    .build();
            return imageRequest;
        }

    }


}
