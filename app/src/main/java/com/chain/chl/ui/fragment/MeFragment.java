package com.chain.chl.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import com.chain.chl.R;

import butterknife.ButterKnife;
import com.chain.chl.manager.CardManager;
import com.chain.chl.model.BaseResponse;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.adapter.MyFavouriteAdapter;
import java.util.ArrayList;
import java.util.List;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by baoyunfeng on 2018/5/19.
 */

public class MeFragment extends Fragment {

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;

    private List<Card> cardList = new ArrayList<>();
    MyFavouriteAdapter recIndexAdapter;
    LinearLayoutManager linearLayoutManager;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardList = new ArrayList<>();
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me, null);
        ButterKnife.bind(this, view);

        loadData(true);
        initViews();

        return view;
    }

    private int offset;

    private void loadData(boolean clear) {
        if(clear){
            offset = 0;
            cardList.clear();
        }
        CardManager.getInstance()
            .getCardFavouriteList(offset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<BaseResponse<List<Card>>>() {
                @Override public void onCompleted() {

                }

                @Override public void onError(Throwable e) {
                    Log.d("RecIndex", "onError: " + e.getMessage());
                }

                @Override public void onNext(BaseResponse<List<Card>> listBaseResponse) {
                    if (listBaseResponse.getCode() == 0) {
                        cardList.addAll(listBaseResponse.getData());
                        recIndexAdapter.notifyDataSetChanged();
                    }
                }
            });
        offset ++;
    }

    private void initViews() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                cardList.clear();
                loadData(true);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        recIndexAdapter = new MyFavouriteAdapter(getContext(), cardList);
        recIndexAdapter.setOnMyFavouriteAdapterListener(new MyFavouriteAdapter.OnMyFavouriteAdapterListener() {
            @Override public void deleteItem(int position) {
                cardList.remove(position);
                recIndexAdapter.notifyDataSetChanged();
            }
        });
        linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(recIndexAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public int lastPosition;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition == recIndexAdapter.getItemCount() - 1 ) {
                    loadData(false);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastPosition = linearLayoutManager.findLastVisibleItemPosition();
            }
        });
    }
}
