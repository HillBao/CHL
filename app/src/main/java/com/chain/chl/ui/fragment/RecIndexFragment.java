package com.chain.chl.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.manager.CardManager;
import com.chain.chl.model.BaseResponse;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.adapter.RecIndexAdapter;
import com.chain.chl.widget.Navigator;
import java.util.ArrayList;
import java.util.List;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by baoyunfeng on 2018/5/19.
 */

public class RecIndexFragment extends Fragment {

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;

    private List<Card> cardList = new ArrayList<>();
    RecIndexAdapter recIndexAdapter;


    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardList = new ArrayList<>();
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recindex, null);
        ButterKnife.bind(this, view);


        initDatas();
        initViews();

        return view;
    }

    private void initDatas(){
        //cardList.add(new Card());
        //cardList.add(new Card());
        //cardList.add(new Card());
        //cardList.add(new Card());
        //cardList.add(new Card());
        //cardList.add(new Card());
        CardManager.getInstance().getCardList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<BaseResponse<List<Card>>>() {
                @Override public void onCompleted() {

                }

                @Override public void onError(Throwable e) {
                    Log.d("RecIndex", "onError: "+e.getMessage());
                }

                @Override public void onNext(BaseResponse<List<Card>> listBaseResponse) {
                    if(listBaseResponse.getCode()==0){
                        cardList.addAll(listBaseResponse.getData());
                        recIndexAdapter.notifyDataSetChanged();
                    }
                }
            });
    }

    private void initViews(){
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                initDatas();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        recIndexAdapter = new RecIndexAdapter(getContext(),cardList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(recIndexAdapter);
    }
}
