package com.chain.chl.ui.viewholder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.manager.CardManager;
import com.chain.chl.model.BaseResponse;
import com.chain.chl.model.card.Card;
import com.chain.chl.model.card.TxtImgCard;
import com.chain.chl.ui.activity.TxtImgDetailActivity;
import com.chain.chl.util.FrescoLoadUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TxtImgViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rootView) View rootView;
    @BindView(R.id.title) TextView textView;
    @BindView(R.id.simpleDraweeView) SimpleDraweeView simpleDraweeView;
    @BindView(R.id.content) TextView content;

    private Context mContext;

    public static TxtImgViewHolder createViewHolder(ViewGroup parentView) {
        View view = LayoutInflater.from(parentView.getContext())
            .inflate(R.layout.viewholder_rec_txtimg, parentView, false);
        return new TxtImgViewHolder(view);
    }

    public TxtImgViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    AlertDialog alertDialog;

    public void setData(final Card card) {
        textView.setText(card.getTitle());
        FrescoLoadUtils.Builder.newBuilderWithDraweeView(simpleDraweeView)
            .setUriStr(card.getCover())
            .build();
        if (card.getTxtImg() instanceof TxtImgCard) {
            TxtImgCard txtImgCard = card.getTxtImg();
            content.setText(txtImgCard.getContent());
        }
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                TxtImgDetailActivity.open(mContext, card);
            }
        });
        rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override public boolean onLongClick(View v) {
                if (alertDialog == null) {
                    alertDialog = new AlertDialog.Builder(itemView.getContext()).setTitle("是否取消收藏")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        })
                        .setNegativeButton("确认", new DialogInterface.OnClickListener() {
                            @Override public void onClick(DialogInterface dialog, int which) {
                                CardManager.getInstance()
                                    .removeFavouriteCard(card.getCardId())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Observer<BaseResponse>() {
                                        @Override public void onCompleted() {

                                        }

                                        @Override public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onNext(BaseResponse listBaseResponse) {

                                        }
                                    });
                                if (onTxtImgViewHolderListener != null) {
                                    onTxtImgViewHolderListener.deleteItem();
                                }
                                alertDialog.dismiss();
                            }
                        })
                        .create();
                }
                alertDialog.show();
                return true;
            }
        });
    }

    private OnTxtImgViewHolderListener onTxtImgViewHolderListener;

    public void setOnTxtImgViewHolderListener(
        OnTxtImgViewHolderListener onTxtImgViewHolderListener) {
        this.onTxtImgViewHolderListener = onTxtImgViewHolderListener;
    }

    public interface OnTxtImgViewHolderListener {
        void deleteItem();
    }
}
