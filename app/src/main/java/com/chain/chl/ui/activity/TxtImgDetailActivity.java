package com.chain.chl.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.base.BaseActivity;
import com.chain.chl.model.card.Card;
import com.chain.chl.util.FrescoLoadUtils;
import com.chain.chl.widget.Navigator;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.Serializable;

public class TxtImgDetailActivity extends BaseActivity {

    private static String INTENT_CARD = "card";
    private Card preCard;

    @BindView(R.id.navigator) Navigator navigator;

    @BindView(R.id.simpleDraweeView) SimpleDraweeView simpleDraweeView;
    @BindView(R.id.content) TextView content;

    public static void open(Context context,Card card){
        Intent intent = new Intent(context,TxtImgDetailActivity.class);
        intent.putExtra(INTENT_CARD, (Serializable) card);
        context.startActivity(intent);
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_txtimg_detail);
        ButterKnife.bind(this);
        preCard = (Card)getIntent().getSerializableExtra(INTENT_CARD);

        initViews();
    }

    private void initViews() {

        navigator.setOnNavigatorListener(new Navigator.OnNavigatorListener() {
            @Override public void onBack() {
                finish();
            }
        });
        navigator.setTitle(preCard.getTitle());

        FrescoLoadUtils.Builder.newBuilderWithDraweeView(simpleDraweeView)
            .setUriStr(preCard.getCover())
            .build();
        content.setText(preCard.getTxtImg().getContent());

    }
}
