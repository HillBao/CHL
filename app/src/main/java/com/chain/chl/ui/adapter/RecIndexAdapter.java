package com.chain.chl.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.viewholder.TxtImgViewHolder;
import java.util.ArrayList;
import java.util.List;

public class RecIndexAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<Card> cardList = new ArrayList<>();

    public RecIndexAdapter(Context context,List<Card> cardList){
        this.mContext= context;
        this.cardList = cardList;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return TxtImgViewHolder.createViewHolder(parent);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == Card.CardType.TxtImg){
            ((TxtImgViewHolder)holder).setData(cardList.get(position));
        }
    }

    @Override public int getItemViewType(int position) {
        return cardList.get(position).getCardType();
    }

    @Override public int getItemCount() {
        return cardList.size();
    }
}
