package com.chain.chl.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.viewholder.CardViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.List;

/**
 * Created by mac on 16/6/6.
 */
public class PhotoCardAdapter extends BaseAdapter {

    private Context mContext;
    private List<Card> mData;

    public PhotoCardAdapter(Context context, List<Card> mData) {
        this.mContext = context;
        this.mData = mData;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Card card = mData.get(position);


        CardViewHolder viewHolder = null;
        if(convertView== null){
            viewHolder = CardViewHolder.createViewHolder(parent);
            convertView = viewHolder.getRootView();
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (CardViewHolder) convertView.getTag();
        }

        viewHolder.setData(card);

        return convertView;
    }
}
