package com.chain.chl.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.viewholder.MyFavouriteTopViewHolder;
import com.chain.chl.ui.viewholder.TxtImgViewHolder;
import java.util.ArrayList;
import java.util.List;

public class MyFavouriteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int ITEMTYPE_HEAD = 10001;

    private Context mContext;
    private List<Card> cardList = new ArrayList<>();

    public MyFavouriteAdapter(Context context,List<Card> cardList){
        this.mContext= context;
        this.cardList = cardList;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEMTYPE_HEAD){
            return MyFavouriteTopViewHolder.createViewHolder(parent);
        }
        return TxtImgViewHolder.createViewHolder(parent);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(ITEMTYPE_HEAD == holder.getItemViewType()){

        }else if(holder.getItemViewType() == Card.CardType.TxtImg){
            final int currentPosition = position - 1;
            ((TxtImgViewHolder)holder).setData(cardList.get(currentPosition));
            ((TxtImgViewHolder)holder).setOnTxtImgViewHolderListener(new TxtImgViewHolder.OnTxtImgViewHolderListener() {
                @Override public void deleteItem() {
                    if(onMyFavouriteAdapterListener!=null){
                        onMyFavouriteAdapterListener.deleteItem(currentPosition);
                    }
                }
            });
        }
    }

    @Override public int getItemViewType(int position) {
        if(position == 0){
            return ITEMTYPE_HEAD;
        }
        return cardList.get(position-1).getCardType();
    }

    @Override public int getItemCount() {
        return cardList.size()+1;
    }

    private OnMyFavouriteAdapterListener onMyFavouriteAdapterListener;

    public void setOnMyFavouriteAdapterListener(
        OnMyFavouriteAdapterListener onMyFavouriteAdapterListener) {
        this.onMyFavouriteAdapterListener = onMyFavouriteAdapterListener;
    }

    public interface OnMyFavouriteAdapterListener{
        void deleteItem(int position);
    }
}
