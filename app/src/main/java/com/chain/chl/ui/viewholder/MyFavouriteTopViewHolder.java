package com.chain.chl.ui.viewholder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.model.card.Card;
import com.chain.chl.model.card.TxtImgCard;
import com.chain.chl.ui.activity.AboutUsActivity;
import com.chain.chl.ui.activity.SaveWalletActivity;
import com.chain.chl.ui.activity.TxtImgDetailActivity;
import com.chain.chl.util.FrescoLoadUtils;
import com.facebook.drawee.view.SimpleDraweeView;

public class MyFavouriteTopViewHolder extends RecyclerView.ViewHolder{

    //@BindView(R.id.simpleDraweeView) SimpleDraweeView headImage;

    @BindView(R.id.aboutus)
    TextView aboutus;
    @BindView(R.id.savewallet)
    TextView savewallet;

    private Context mContext;

    public static MyFavouriteTopViewHolder createViewHolder(ViewGroup parentView){
        View view = LayoutInflater.from(parentView.getContext()).inflate(R.layout.viewholder_myfavouritetop,parentView,false);
        return new MyFavouriteTopViewHolder(view);
    }


    public MyFavouriteTopViewHolder(final View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        ButterKnife.bind(this,itemView);

        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                AboutUsActivity.open(itemView.getContext());
            }
        });
        savewallet.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                SaveWalletActivity.open(itemView.getContext());
            }
        });
/*
        FrescoLoadUtils.Builder.newBuilderWithDraweeView(headImage)
            .setPlaceholderDrawable(ContextCompat.getDrawable(itemView.getContext(),R.drawable.ic_launcher))
            .build();*/
    }
}
