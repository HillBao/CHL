package com.chain.chl.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.os.Bundle;
import android.view.View;

import android.widget.TabHost;
import com.chain.chl.R;
import com.chain.chl.model.TabConfig;
import com.chain.chl.ui.fragment.CardSwipeFragment;
import com.chain.chl.ui.fragment.MeFragment;
import com.chain.chl.ui.fragment.RecIndexFragment;
import com.chain.chl.widget.Tab;

import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity {

    @BindView(R.id.tabhost) FragmentTabHost mTabHost;

    private List<Tab> tabList = new ArrayList<>();

    public static void open(Activity context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
        setupTabHost();

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override public void onTabChanged(String tabId) {
                for (Tab tab : tabList) {
                    if (tab.getTag().equals(tabId)) {
                        tab.setState(true);
                    } else {
                        tab.setState(false);
                    }
                }
            }
        });
    }

    private void setupTabHost() {
        //addTab(Tab.TabType.REC, SlideFragment.class);
        addTab(Tab.TabType.REC, CardSwipeFragment.class);
        addTab(Tab.TabType.ME, MeFragment.class);
    }

    private void addTab(@Tab.TabType String tabType, Class fragmentClass) {
        TabConfig config = new TabConfig(tabType);
        View view = createTabView(this, config);
        mTabHost.addTab(mTabHost.newTabSpec(tabType).setIndicator(view), fragmentClass, null);
    }

    private View createTabView(Context context, TabConfig tabConfig) {
        Tab tab = new Tab(getBaseContext());
        tab.init(tabConfig);
        tabList.add(tab);
        //        ViewConfig viewConfig = new ViewConfig();
        //        viewConfig.tabConfig = tabConfig;
        //        ButterKnife.bind(viewConfig, view);
        //        viewConfig.imageView.setImageDrawable(viewConfig.tabConfig.getImageDrawable(context));
        //        viewConfig.tabName.setText(viewConfig.tabConfig.getTextString(context));
        //        tabList.add(viewConfig);
        return tab;
    }
}
