package com.chain.chl.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.base.BaseActivity;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.rootView) View rootView;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        rootView.postDelayed(new Runnable() {
            @Override public void run() {
                MainActivity.open(SplashActivity.this);
                finish();
            }
        },1000);
    }
}
