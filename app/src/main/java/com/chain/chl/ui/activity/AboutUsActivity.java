package com.chain.chl.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.base.BaseActivity;
import com.chain.chl.model.card.Card;
import com.chain.chl.util.FrescoLoadUtils;
import com.chain.chl.widget.Navigator;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.Serializable;

public class AboutUsActivity extends BaseActivity {

    private static String INTENT_CARD = "card";
    private Card preCard;

    @BindView(R.id.navigator) Navigator navigator;

    @BindView(R.id.simpleDraweeView) SimpleDraweeView simpleDraweeView;
    @BindView(R.id.content) TextView content;

    public static void open(Context context){
        Intent intent = new Intent(context,AboutUsActivity.class);
        context.startActivity(intent);
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_txtimg_detail);
        ButterKnife.bind(this);
        preCard = (Card)getIntent().getSerializableExtra(INTENT_CARD);

        initViews();
    }

    private void initViews() {

        navigator.setOnNavigatorListener(new Navigator.OnNavigatorListener() {
            @Override public void onBack() {
                finish();
            }
        });
        navigator.setTitle("关于我们");

        FrescoLoadUtils.Builder.newBuilderWithDraweeView(simpleDraweeView)
            .setPlaceholderDrawable(ContextCompat.getDrawable(getBaseContext(),R.drawable.ic_launcher))
            .build();
        content.setText("开发说明：\n"
            + "\n"
            + "（1）一言不合就发nas：根据用户打开次数和停留时长等参数，不定期往用户钱包地址空投nas。\n"
            + "\n"
            + "（2）文字信息和视频信息上链存储，保证公开有效。详见本app对应智能合约。并支持调用合约存储方法将数据上链。\n"
            + "\n"
            + "（3）后续还会有基于人工智能推荐的高级功能上线。");

    }
}
