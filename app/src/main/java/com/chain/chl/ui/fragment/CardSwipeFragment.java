package com.chain.chl.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.manager.CardManager;
import com.chain.chl.model.BaseResponse;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.activity.TxtImgDetailActivity;
import com.chain.chl.ui.adapter.PhotoCardAdapter;
import com.chain.chl.widget.swipe.SwipeFlingAdapterView;
import java.util.ArrayList;
import java.util.List;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by baoyunfeng on 2018/5/19.
 */

public class CardSwipeFragment extends Fragment {

    @BindView(R.id.rootView) View rootView;

    private static final String TAG = "CardSwipeFragment";


    //核心控件
    @BindView(R.id.flingContainer)
    SwipeFlingAdapterView flingContainer;
    PhotoCardAdapter photoCardAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cardswipe, null);
        ButterKnife.bind(this, view);

        initViews();
        return view;
    }


    List<Card> list = new ArrayList<>();
    private void initViews(){
        flingContainer.setClipChildren(false);
        flingContainer.setClipToPadding(false);
        refreshData();

        photoCardAdapter = new PhotoCardAdapter(getContext(),list);
        flingContainer.setAdapter(photoCardAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override public void removeFirstObjectInAdapter() {
                if (list.size() != 0) {
                    list.remove(0);
                    photoCardAdapter.notifyDataSetChanged();
                }
            }

            @Override public void onLeftCardExit(Object dataObject) {

            }

            @Override public void onRightCardExit(Object dataObject) {
                String cardId;
                if(dataObject instanceof Card){
                    cardId = ((Card) dataObject).getCardId();
                }else{
                    return;
                }
                CardManager.getInstance().addFavouriteCard(cardId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<BaseResponse>() {
                        @Override public void onCompleted() {

                        }

                        @Override public void onError(Throwable e) {
                            Log.d("RecIndex", "onError: "+e.getMessage());
                        }

                        @Override public void onNext(BaseResponse listBaseResponse) {

                        }
                    });
            }

            @Override public void onAdapterAboutToEmpty(int itemsInAdapter) {
                refreshData();
            }

            @Override public void onAdapterHasEmpty() {

            }

            @Override public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                if (view != null) {
                    View swipeRightView = view.findViewById(R.id.item_swipe_right_indicator);
                    if (swipeRightView != null) {
                        swipeRightView.setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                    }
                    View swipeLeftView = view.findViewById(R.id.item_swipe_left_indicator);
                    if (swipeLeftView != null) {
                        swipeLeftView.setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                    }
                }
            }

            @Override public void accessMatchShareFragment() {

            }

            @Override public boolean getHasFinishDetailCard() {
                return true;
            }
        });
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                TxtImgDetailActivity.open(getContext(),list.get(itemPosition));
            }
        });
    }

    public void refreshData(){
        CardManager.getInstance().getCardList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<BaseResponse<List<Card>>>() {
                @Override public void onCompleted() {

                }

                @Override public void onError(Throwable e) {
                    Log.d("RecIndex", "onError: "+e.getMessage());
                }

                @Override public void onNext(BaseResponse<List<Card>> listBaseResponse) {
                    if(listBaseResponse.getCode()==0){
                        list.addAll(listBaseResponse.getData());
                        photoCardAdapter.notifyDataSetChanged();
                    }
                }
            });
    }
}
