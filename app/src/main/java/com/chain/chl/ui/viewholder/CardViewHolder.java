package com.chain.chl.ui.viewholder;

import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.model.card.Card;
import com.chain.chl.ui.activity.TxtImgDetailActivity;
import com.chain.chl.ui.adapter.PhotoCardAdapter;
import com.chain.chl.util.FrescoLoadUtils;
import com.facebook.drawee.view.SimpleDraweeView;

public class CardViewHolder {

    public static CardViewHolder createViewHolder(
        ViewGroup parent){View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_slide, parent, false);
        return new CardViewHolder(view);
    }


    @BindView(R.id.img_bg) SimpleDraweeView imgBg;
    @BindView(R.id.img_user) SimpleDraweeView userIcon;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_user_say) TextView userSay;
    @BindView(R.id.rootView) View rootView;

    public CardViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }

    public void setData(final Card bean){
        CardViewHolder holder = this;
        FrescoLoadUtils.Builder.newBuilderWithDraweeView(holder.imgBg)
            .setUriStr(bean.getCover())
            .build();
        FrescoLoadUtils.Builder.newBuilderWithDraweeView(holder.userIcon)
            .setUriStr(bean.getCover())
            .build();
        holder.tvTitle.setText(bean.getTitle());
        holder.userSay.setText(bean.getTxtImg().getContent());
    }

    public View getRootView() {
        return rootView;
    }
}
