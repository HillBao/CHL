package com.chain.chl.service;

import com.chain.chl.model.BaseResponse;
import com.chain.chl.model.card.Card;
import com.google.gson.JsonObject;
import java.util.List;
import java.util.Observer;
import okhttp3.RequestBody;
import org.json.JSONObject;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface CardService {
    @POST("card/get_list") Observable<BaseResponse<List<Card>>> getCardList(@Body JsonObject object,
        @Query("deviceId") String deviceId,@Query("appType") int appType);


    @POST("card/get_favorite_list") Observable<BaseResponse<List<Card>>> getFavouriteCardList(@Body JsonObject object,
        @Query("deviceId") String deviceId,@Query("appType") int appType);

    @POST("card/add_favorite") Observable<BaseResponse> addFavouriteCard(@Body JsonObject object,
        @Query("deviceId") String deviceId,@Query("appType") int appType);


    @POST("card/remove_favorite") Observable<BaseResponse> removeFavouriteCard(@Body JsonObject object,
        @Query("deviceId") String deviceId,@Query("appType") int appType);


    @GET("user/save_wallet_address") Observable<BaseResponse> saveWalletAddress(
        @Query("deviceId") String deviceId,@Query("appType") int appType,@Query("walletAddress") String walletAddress);

}
