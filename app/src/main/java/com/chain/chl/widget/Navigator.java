package com.chain.chl.widget;

import android.content.Context;
import android.support.annotation.StringDef;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chain.chl.R;
import com.chain.chl.model.TabConfig;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by baoyunfeng on 2018/5/19.
 */

public class Navigator extends RelativeLayout {

    private Context mContext;

    @BindView(R.id.title) TextView mTitleView;
    @BindView(R.id.back) View back;

    public Navigator(Context context) {
        this(context, null);
    }

    public Navigator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Navigator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        LayoutInflater.from(getContext()).inflate(R.layout.view_navigator, this);
        ButterKnife.bind(this, this);
        back.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if (onNavigatorListener != null) {
                    onNavigatorListener.onBack();
                }
            }
        });
    }

    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    public View getBack() {
        return back;
    }

    private OnNavigatorListener onNavigatorListener;

    public void setOnNavigatorListener(OnNavigatorListener onNavigatorListener) {
        this.onNavigatorListener = onNavigatorListener;
    }

    public interface OnNavigatorListener {
        void onBack();
    }
}
