package com.chain.chl.widget;

import android.content.Context;
import android.support.annotation.StringDef;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chain.chl.R;
import com.chain.chl.model.TabConfig;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baoyunfeng on 2018/5/19.
 */

public class Tab extends RelativeLayout {

    @BindView(R.id.tab_name) TextView tabName;
    @BindView(R.id.imageview) ImageView imageView;

    @Retention(RetentionPolicy.SOURCE) @StringDef({ TabType.REC, TabType.ME })
    public @interface TabType {
        String REC = "rec";
        String ME = "me";
    }

    private Context mContext;

    private TabConfig tabConfig;

    public Tab(Context context) {
        this(context, null);
    }

    public Tab(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Tab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        LayoutInflater.from(getContext()).inflate(R.layout.view_tab_item, this);
        ButterKnife.bind(this, this);
    }

    public void init(TabConfig tabConfig) {
        this.tabConfig = tabConfig;
        tabName.setText(tabConfig.getName());
        imageView.setImageDrawable(ContextCompat.getDrawable(mContext, tabConfig.getIcon()));
    }

    public void setState(boolean select) {
        if (select) {
            imageView.setSelected(true);
        } else {
            imageView.setSelected(false);
        }
    }

    public String getTag() {
        if (tabConfig != null) {
            return tabConfig.getTag();
        }
        return null;
    }
}
