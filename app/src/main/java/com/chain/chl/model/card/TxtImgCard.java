package com.chain.chl.model.card;

/**
 * 字段	类型	说明
 content	string	详细内容（不用富文本的话，分段怎么处理？）

 */
public class TxtImgCard {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
