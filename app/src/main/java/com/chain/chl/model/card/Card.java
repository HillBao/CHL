package com.chain.chl.model.card;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
*
cardId	string	card标识
cardType	int32	card类型
title	string	名称
cover	string	封面
tags	list	标签
description	string	描述
author	UserInfo	用户信息（第一版可以先不加）
cardDetail	object	不同类型的卡片，详情不一样
viewCount	i32	观看次数（第一版可以先不加此功能）
commentCount	i32	评论次数（第一版可以先不加此功能）
favoriteCount	i32	收藏次数（第一版可以先不加此功能）
recommendMeta	object	推荐信息（第一版可以先不加此功能）

* */
public class Card implements Serializable,Parcelable {

    private static final String TAG = "CardX";

    protected Card(Parcel in) {
        cardId = in.readString();
        cardType = in.readInt();
        title = in.readString();
        cover = in.readString();
        description = in.readString();
        cardDetail = new Gson().fromJson(in.readString(),JsonObject.class);
    }

    public static final Creator<Card> CREATOR = new Creator<Card>() {
        @Override public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        @Override public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cardId);
        dest.writeInt(cardType);
        dest.writeString(title);
        dest.writeString(cover);
        dest.writeString(description);
        dest.writeString(cardDetail.toString());
    }

    /**
     * 0	视频card
     * 1	普通文本card
     */
    @Retention(RetentionPolicy.SOURCE) @IntDef({ Card.CardType.Video, Card.CardType.TxtImg })
    public @interface CardType {
        int Video = 0;
        int TxtImg = 1;
    }

    private String cardId;
    private int cardType;
    private String title;
    private String cover;
    private String description;
    private JsonObject cardDetail;

    public Card() {
        cardId = "2fvzde";
        cardType = Card.CardType.TxtImg ;
        title = "长征四号丙运载火箭升空";
        cover = "http://file.izuiyou.com/img/view/id/247148853/sz/src";
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("content","雷锋网 AI 科技评论按：国际机器人与自动化会议（ICRA）是 IEEE 机器人与自动化学会（IEEE Robotics and Automation Society）的旗舰会议，同时也是机器人研究者展示他们工作的重要论坛。ICRA 成立于 1984 年，每年举行一次，该会议将机器人和自动化领域的学者专家聚集起来，使他们能够通过展示和讨论科研成果进行技术交流。ICRA 为所有参会的代表营造了一个非常好的环境，让他们能够沉浸在机器人和自动化科学技术的前沿研究之中。ICRA 2018 于近期在布里斯班举行，雷锋网 AI 科技评论也于澳大利亚现场带来更多精彩报道。\n"
            + "\n"
            + "与以往一样，除了学术论文交流的环节，本届 ICRA 也举办了规模宏大、类型丰富的机器人挑战赛。具体而言，ICRA 2018 机器人挑战赛由五项赛事组成，它们是：\n"
            + "\n"
            + "2018 可移动微型机器人挑战赛（Mobile microrobotics challenge 2018）\n"
            + "房间整理家务机器人挑战赛（Tidy up my room challenge）\n"
            + "软体机器人挑战赛（Soft material robot challenge）\n"
            + "机器人初创公司启动大赛（Robot launch startup competition）\n"
            + "大疆创新 ROBOMASTER 人工智能挑战赛\n"
            + "\n"
            + "以下是比赛的详细介绍：\n"
            + "\n"
            + "\n"
            + "2018 可移动微型机器人挑战赛（Mobile microrobotics challenge 2018）\n"
            + "\n"
            + "组织者：David J. Cappelleri, 普渡大学Aaron T. Ohta, 夏威夷大学 Igor Paprotny, 伊利诺伊大学-芝加哥分校 Dan Popa, 路易斯维尔大学\n"
            + "\n"
            + "大赛主页：\n"
            + "\n"
            + "IEEE 机器人与自动化学会（RAS）微型机器人与自动化技术专委会（MNRA）组织了 2018 年可移动微型机器人挑战赛，在这项赛事中，人类头发直径大小级别的微型机器人将在装配工作的自主性、准确性测试中进行挑战。\n"
            + "\n"
            + "参赛队伍最多可以参加以下三个项目：\n"
            + "\n"
            + "1.自动化操作和准确性挑战赛：\n"
            + "\n"
            + "微型机器人必须自动操作固定障碍物周围的微型零件，使其到达人们所期望的位置、具有正确的朝向，并且将它叠放在基板上。参赛者需要在最短的时间内，尽可能精确地将物体以正确的朝向运送到目标位置上去。\n"
            + "\n"
            + "2.微型组装挑战赛：\n"
            + "\n"
            + "微型机器人必须在规定时间内在一个狭窄的通道内组装多个微型零件。这项任务模拟了可以预见到的微型组装的应用，包括人体血管内的操作和纳米加工中的零件装配。\n"
            + "\n"
            + "3.MMC 与海报展示：\n"
            + "\n"
            + "每个团队都有机会展示并说明任何他们的微型机器人系统的先进的性能和功能。每支参赛队都将获得一张选票，用来决出展示环节最佳的优胜者。");
        cardDetail= jsonObject;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public <T> T getCardDetail() {
        Gson gson = new Gson();
        if(cardType == CardType.TxtImg){
            return (T)gson.fromJson(cardDetail,TxtImgCard.class);
        }
        return null;
    }

    public TxtImgCard getTxtImg(){
        TxtImgCard card = getCardDetail();
        return card == null ? new TxtImgCard():card ;
    }

    public void setCardDetail(JsonObject cardDetail) {
        this.cardDetail = cardDetail;
    }
}
