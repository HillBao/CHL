package com.chain.chl.model;

import com.chain.chl.R;
import com.chain.chl.widget.Tab;

/**
 * Created by baoyunfeng on 2018/5/19.
 */

public class TabConfig {

    private String name;
    private int icon;
    private String tag;

    public TabConfig(@Tab.TabType String tabId){
        switch (tabId){
            case Tab.TabType.ME:
                setConfig("我的",R.drawable.ic_tab_me,Tab.TabType.ME);
                break;
            case Tab.TabType.REC:
                setConfig("推荐",R.drawable.ic_tab_home,Tab.TabType.REC);
                break;
        }
    }


    public void setConfig(String name, int icon,String tag) {
        this.name = name;
        this.icon = icon;
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public int getIcon() {
        return icon;
    }

    public String getTag() {
        return tag;
    }
}
