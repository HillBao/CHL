package com.chain.chl.manager;

import android.app.Application;
import baoyunfs.deviceid.DeviceIdManager;
import com.chain.chl.ServerHelper;
import com.chain.chl.model.BaseResponse;
import com.chain.chl.model.card.Card;
import com.chain.chl.service.CardService;
import com.google.gson.JsonObject;
import java.util.List;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class CardManager {
    private volatile static CardManager instance;

    public static CardManager getInstance() {
        if (instance == null) {
            synchronized (CardManager.class) {
                if (instance == null) {
                    instance = new CardManager();
                }
            }
        }
        return instance;
    }

    private CardService service;

    private CardManager() {
        Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .baseUrl(ServerHelper.baseUrl)
            .build();
        service = retrofit.create(CardService.class);
    }


    public Observable<BaseResponse<List<Card>>> getCardList() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("loadType",0);
        jsonObject.addProperty("cardType",1);
        return service.getCardList(jsonObject,
            DeviceIdManager.getInstance().getDeviceId(),1);
    }




    public Observable<BaseResponse<List<Card>>> getCardFavouriteList(int offset) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("loadType",0);
        jsonObject.addProperty("cardType",1);

        int size = 20;
        JsonObject paging = new JsonObject();
        paging.addProperty("offset",offset * size);
        paging.addProperty("size",size);
        jsonObject.add("paging", paging);

        return service.getFavouriteCardList(jsonObject,
            DeviceIdManager.getInstance().getDeviceId(),1);
    }




    public Observable<BaseResponse> addFavouriteCard(String id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cardId",id);
        jsonObject.addProperty("cardType",1);
        return service.addFavouriteCard(jsonObject,
            DeviceIdManager.getInstance().getDeviceId(),1);
    }



    public Observable<BaseResponse> removeFavouriteCard(String id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cardId",id);
        jsonObject.addProperty("cardType",1);
        return service.removeFavouriteCard(jsonObject,
            DeviceIdManager.getInstance().getDeviceId(),1);
    }



    public Observable<BaseResponse> saveWalletAddress(String address) {
        return service.saveWalletAddress(
            DeviceIdManager.getInstance().getDeviceId(),1,address);
    }
}
