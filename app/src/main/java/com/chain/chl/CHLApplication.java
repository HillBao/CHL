package com.chain.chl;

import android.app.Application;
import android.util.Log;
import baoyunfs.deviceid.DeviceIdManager;
import com.chain.chl.manager.CardManager;
import com.chain.chl.model.card.Card;
import com.facebook.drawee.backends.pipeline.Fresco;
import java.util.List;
import java.util.concurrent.ExecutorService;
import rx.Observer;
import rx.schedulers.Schedulers;

public class CHLApplication extends Application {

    private static final String TAG = "DeviceIdManagerX";

    @Override public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        Fresco.initialize(CHLApplication.this);

        DeviceIdManager.getInstance()
            .setmOnDeviceIdManagerListener(new DeviceIdManager.OnDeviceIdManagerListener() {

                @Override public boolean sdCardUseable() {
                    return false;
                }

                @Override public ExecutorService getExecutorService() {
                    return null;
                }

                @Override public void failureOfGetAndroidID() {

                }

                @Override public void successOfGetAndroidID() {

                }
            });
        DeviceIdManager.getInstance()
            .getDeviceId(getApplicationContext(), new DeviceIdManager.OnDeviceIdCallBack() {

                @Override public void callBackDeviceId(String deviceId) {
                    Log.d(TAG, "callBackDeviceId: " + deviceId);
                }
            });
    }
}
